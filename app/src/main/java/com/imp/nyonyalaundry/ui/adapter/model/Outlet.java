package com.imp.nyonyalaundry.ui.adapter.model;

/**
 * Created by Owner on 6/28/2016.
 */
public class Outlet {

    private String name;
    private String address;


    public Outlet(String name, String addres){
        this.name = name;
        this.address = addres;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
