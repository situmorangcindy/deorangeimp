package com.imp.nyonyalaundry.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.Pref;

public class VerificationActivity extends AppCompatActivity {

    private EditText verificationNumberOne, verificationNumberTwo, verificationNumberThree, verificationNumberFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String token = Pref.readString(VerificationActivity.this.getApplicationContext(), Pref.TOKEN);
        if (!token.isEmpty()) {
            finish();
            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_verification);
        verificationNumberOne = (EditText) findViewById(R.id.verificationNumber_one);
        verificationNumberTwo = (EditText) findViewById(R.id.verificationNumber_two);
        verificationNumberThree = (EditText) findViewById(R.id.verificationNumber_three);
        verificationNumberFour = (EditText) findViewById(R.id.verificationNumber_four);
        findViewById(R.id.verifyButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new verifyPhoneNumber().execute();
                startActivity(new Intent(VerificationActivity.this, MainActivity.class));
                finish();
            }
        });

        changeStatusBarColor();
    }
    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, VerificationActivity.class));
                finish();
//                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
