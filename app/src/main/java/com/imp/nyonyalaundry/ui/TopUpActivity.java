package com.imp.nyonyalaundry.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.imp.nyonyalaundry.R;

/**
 * Created by Owner on 7/5/2016.
 */
public class TopUpActivity extends AppCompatActivity implements View.OnClickListener{
    private Toolbar toolbar;
    private Button topUpButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);
        toolbar = (Toolbar) findViewById(R.id.top_up_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Top Up Saldo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        topUpButton = (Button) findViewById(R.id.top_up_help_button);
        topUpButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.top_up_help_button:
                startActivity(new Intent(this, HelpActivity.class));
                break;
        }
    }
}
