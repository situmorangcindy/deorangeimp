package com.imp.nyonyalaundry.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.helper.Pref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Owner on 3/17/2016.
 */
public class PermissionSettingActivity extends AppCompatActivity {
    private CheckBox storageCheckBox, contactCheckBox, cameraCheckBox, locationCheckBox;
    private boolean storageApproved = false;
    private boolean contactApproved = false;
    private boolean cameraApproved = false;
    private boolean locationApproved = false;
    private List<Boolean> totalApproved = new ArrayList<Boolean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT < 23) nextPage();
        else if (isAllPermissionsApproved()) nextPage();

        setContentView(R.layout.activity_permission_setting);
        storageCheckBox = (CheckBox) findViewById(R.id.permission_setting_external_storage_check_box);
        contactCheckBox = (CheckBox) findViewById(R.id.permission_setting_contact_check_box);
        cameraCheckBox = (CheckBox) findViewById(R.id.permission_setting_camera_check_box);
        locationCheckBox = (CheckBox) findViewById(R.id.permission_setting_location_check_box);

        storageCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (storageCheckBox.isChecked()) storagePermission();
            }
        });
        contactCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (contactCheckBox.isChecked()) contactPermission();
            }
        });

        cameraCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cameraCheckBox.isChecked()) cameraPermission();
            }
        });

        locationCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (locationCheckBox.isChecked()) locationPermission();
            }
        });

    }

    private void contactPermission() {
        List<String> listPermissionsNeeded = new ArrayList<String>();

        int readPhoneState = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_PHONE_STATE);
        int readContact = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_CONTACTS);
        int writeContact = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.WRITE_CONTACTS);

        if (readPhoneState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (readContact != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (writeContact != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            contactApproved = true;
            ActivityCompat.requestPermissions(PermissionSettingActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), AppConstants.KEY_REQUEST_PERMISSION_MARSHMALLOW);
        }
    }

    private void cameraPermission() {
        List<String> listPermissionsNeeded = new ArrayList<String>();

        int camera = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.CAMERA);
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            cameraApproved = true;
            ActivityCompat.requestPermissions(PermissionSettingActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), AppConstants.KEY_REQUEST_PERMISSION_MARSHMALLOW);
        }
    }

    private void locationPermission() {
        List<String> listPermissionsNeeded = new ArrayList<String>();

        int fineLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);

        if (fineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            locationApproved = true;
            ActivityCompat.requestPermissions(PermissionSettingActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), AppConstants.KEY_REQUEST_PERMISSION_MARSHMALLOW);
        }
    }

    private void storagePermission() {
        List<String> listPermissionsNeeded = new ArrayList<String>();

        int writeStorage = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writeStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            storageApproved = true;
            ActivityCompat.requestPermissions(PermissionSettingActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), AppConstants.KEY_REQUEST_PERMISSION_MARSHMALLOW);
        }
    }

    private void nextPage() {
        String token = Pref.readString(this.getApplicationContext(), Pref.TOKEN);
        Intent intent;
        if (!token.isEmpty()) {
            intent = new Intent(this, MainActivity.class);

        } else {
            intent = new Intent(this, RegisterActivity.class);
        }
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case AppConstants.KEY_REQUEST_PERMISSION_MARSHMALLOW: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                if (storageApproved) {
                    perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                    perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        totalApproved.add(storageApproved);
                        if (isAllApproved()) nextPage();
                        storageApproved = false;
                    } else {
                        storagePermission();
                    }
                }

                if (contactApproved) {
                    perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                    perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                    perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);

                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        totalApproved.add(contactApproved);
                        if (isAllApproved()) nextPage();
                        contactApproved = false;
                    } else {
                        contactPermission();
                    }
                }

                if (cameraApproved) {
                    perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        totalApproved.add(cameraApproved);
                        if (isAllApproved()) nextPage();
                        cameraApproved = false;
                    } else {
                        cameraPermission();
                    }
                }

                if (locationApproved) {
                    perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                    perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);

                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        totalApproved.add(locationApproved);
                        if (isAllApproved()) nextPage();
                        locationApproved = false;
                    } else {
                        locationPermission();
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean isAllApproved() {
        boolean isApproved = false;
        if (totalApproved.size() == 4) {
            for (Boolean approved : totalApproved) {
                isApproved = approved;
            }
        }

        return isApproved;
    }

    private boolean isAllPermissionsApproved() {
        boolean isApproved = false;
        int readPhoneState = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_PHONE_STATE);
        int readContact = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_CONTACTS);
        int writeContact = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.WRITE_CONTACTS);
        int camera = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.CAMERA);
        int writeStorage = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int fineLocation = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ContextCompat.checkSelfPermission(PermissionSettingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);

        isApproved = (readPhoneState == PackageManager.PERMISSION_GRANTED) && (readContact == PackageManager.PERMISSION_GRANTED)
                && (writeContact == PackageManager.PERMISSION_GRANTED) && (camera == PackageManager.PERMISSION_GRANTED)
                && (writeStorage == PackageManager.PERMISSION_GRANTED) && (readStorage == PackageManager.PERMISSION_GRANTED
                && (fineLocation == PackageManager.PERMISSION_GRANTED) && (coarseLocation == PackageManager.PERMISSION_GRANTED));
        return isApproved;
    }
}
