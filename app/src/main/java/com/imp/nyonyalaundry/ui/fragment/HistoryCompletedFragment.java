package com.imp.nyonyalaundry.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.ui.model.OrderModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Owner on 6/26/2016.
 */
public class HistoryCompletedFragment extends Fragment {

    private List<OrderModel> orderList = new ArrayList<OrderModel>();
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_history_completed, container, false);
        listView = (ListView) view.findViewById(R.id.history_completed_list_view);
        setUpData();
        return view;
    }

    private void setUpData(){
        orderList.clear();
        for(int i= 1; i< 10; i++) {
            orderList.add(new OrderModel(String.valueOf(i), "Laundry " + i, String.valueOf(i*10000), "WASH"));
        }
        listView.setAdapter(new OrderAdapter(getActivity()));
    }

    class OrderAdapter extends BaseAdapter {
        LayoutInflater inflater;
        Context context;

        public OrderAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(this.context);
        }

        @Override
        public int getCount() {
            return orderList.size();
        }

        @Override
        public Object getItem(int i) {
            return orderList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            MyViewHolder mViewHolder;

            if (view == null) {
                view = inflater.inflate(R.layout.item_completed, viewGroup, false);
                mViewHolder = new MyViewHolder(view);
                view.setTag(mViewHolder);
            } else {
                mViewHolder = (MyViewHolder) view.getTag();
            }

            OrderModel orderModel = orderList.get(position);

            mViewHolder.tvName.setText(orderModel.getOrderShop());
            mViewHolder.tvKodePesan.setText(String.format("Kode Pemesanan : %s", orderModel.getOrderId()));
            mViewHolder.tvTarif.setText(String.format("Tarif : %s", orderModel.getOrderPrice()));

            return view;
        }

        private class MyViewHolder {
            TextView tvName, tvKodePesan, tvTarif;
            ImageView ivIcon;

            public MyViewHolder(View item) {
                tvName = (TextView) item.findViewById(R.id.item_completed_laundry_text_view);
                tvKodePesan = (TextView) item.findViewById(R.id.item_completed_kode_pemesanan_text_view);
                tvTarif = (TextView) item.findViewById(R.id.item_completed_tarif_text_view);
                ivIcon = (ImageView) item.findViewById(R.id.item_completed_avatar_image_view);
            }
        }
    }
}
