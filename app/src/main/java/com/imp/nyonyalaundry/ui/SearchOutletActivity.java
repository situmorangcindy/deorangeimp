package com.imp.nyonyalaundry.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.ui.adapter.OutletListAdapter;
import com.imp.nyonyalaundry.ui.adapter.model.Outlet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 6/28/2016.
 */
public class SearchOutletActivity extends AppCompatActivity{

    private ListView outletListView;
    private OutletListAdapter outletListAdapter;
    private List<Outlet> listOutlet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_outlet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.search_outlet_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        outletListView = (ListView) findViewById(R.id.search_outlet_list_view);

        setAdapter();

    }

    private void setAdapter(){
        listOutlet = new ArrayList<Outlet>();
        listOutlet.add(new Outlet("Langganan Kita ", "Jl. Jendral S Parman No.7 "));
        listOutlet.add(new Outlet("Bersih Laundry ", "Jl. Satria Bima No.10 "));
        listOutlet.add(new Outlet("Harum Laundry ", "Jl. Tangkuban Perahu No.13 "));
        listOutlet.add(new Outlet("Azka Laundry ", "Jl. Sitinurbaya No.1 "));
        listOutlet.add(new Outlet("Dua Saudara Laundry ", "Jl. Rengasdengklok No.4 "));
        listOutlet.add(new Outlet("Mbok Laundry ", "Jl. Jonathan Rifzi No.90 "));
        listOutlet.add(new Outlet("Kilat Laundry ", "Jl. Satria Bima No.3  "));
        listOutlet.add(new Outlet("12 Laundry ", "Jl. Satria Bima No.10  "));
        listOutlet.add(new Outlet("Laundry Kita ", "Jl. Satria Bima No.5  "));
        listOutlet.add(new Outlet("Laundry Murah dan Cepat ", "Jl. Satria Bima No.8  "));

        outletListAdapter = new OutletListAdapter(this, listOutlet);
        outletListView.setAdapter(outletListAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
