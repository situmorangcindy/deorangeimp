package com.imp.nyonyalaundry.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;

/**
 * Created by Owner on 6/27/2016.
 */
public class LandingActivity extends AppCompatActivity implements View.OnClickListener{

    public static final int REGISTER_CUSTOMER = 0;
    public static final int REGISTER_OUTLET = 1;
    public static final int REGISTER_KURIR = 2;

    private Button buttonCustomerLogin;
    private Button buttonDriverLogin;
    private Button buttonOutletLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_landing);

        buttonCustomerLogin = (Button) findViewById(R.id.landing_customer_action_button);
        buttonDriverLogin = (Button) findViewById(R.id.landing_driver_action_button);
        buttonOutletLogin = (Button) findViewById(R.id.landing_outlet_action_button);

        buttonCustomerLogin.setOnClickListener(this);
        buttonDriverLogin.setOnClickListener(this);
        buttonOutletLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.landing_customer_action_button:
                goToRegister(REGISTER_CUSTOMER);
                break;
            case R.id.landing_driver_action_button:
                goToRegister(REGISTER_OUTLET);
                break;
            case R.id.landing_outlet_action_button:
                goToRegister(REGISTER_KURIR);
                break;
        }
    }

    private void goToRegister(int type){
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_REGISTER_TYPE, type);
        startActivity(intent);
        finish();
    }
}
