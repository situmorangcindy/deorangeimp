package com.imp.nyonyalaundry.ui.fragment;

/**
 * Created by Owner on 2/11/2016.
 */
public interface IBaseFragment {

    Integer HOME = 0;
    Integer HISTORY = 1;
    Integer HELP = 2;
    Integer SETTINGS = 3;
}
