package com.imp.nyonyalaundry.ui.fragment;

import android.content.Context;
import android.view.View;
import android.widget.TabHost;

/**
 * Created by Owner on 6/26/2016.
 */
public class DummyTabContent implements TabHost.TabContentFactory {
    private Context mContext;

    public DummyTabContent(Context context){
        mContext = context;
    }

    @Override
    public View createTabContent(String tag) {
        View v = new View(mContext);
        return v;
    }
}