package com.imp.nyonyalaundry.ui;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Owner on 6/28/2016.
 */
public class OutletActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private String nameOutlet;
    private String addressOutlet;

    //UI References
    private EditText editTextDate;
    private EditText editTextTime;
    private int mHour = 0;
    private int mMinute = 0;

    // Reminder Date
    private Date reminderDate;

    // Calendar
    private final Calendar calendar = Calendar.getInstance();
    private final int curDt = calendar.get(Calendar.DAY_OF_MONTH);
    private final int curMt = calendar.get(Calendar.MONTH);
    private final int curYt = calendar.get(Calendar.YEAR);

    private TextView nameOutletTextView;
    private TextView addressOutletTextView;
    private Spinner spinnerPaket;
    private Spinner spinnerPewangi;
    private Toolbar toolbar;
    private LinearLayout callContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);
        Locale current = getResources().getConfiguration().locale;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", current);


        toolbar = (Toolbar) findViewById(R.id.outlet_toolbar);
        setSupportActionBar(toolbar);
        nameOutletTextView = (TextView) findViewById(R.id.outlet_name_text_view);
        addressOutletTextView = (TextView) findViewById(R.id.outlet_address_text_view);
        editTextDate = (EditText) findViewById(R.id.editTextDate);
        editTextTime = (EditText) findViewById(R.id.editTextTime);
        callContainer = (LinearLayout) findViewById(R.id.outlet_call_linear_layout);
        callContainer.setOnClickListener(this);

        editTextDate.setText(dateFormat.format(new Date()));
        editTextTime.setText(String.format("%02d:%02d", mHour, mMinute));

        editTextDate.setOnClickListener(this);
        editTextTime.setOnClickListener(this);

        spinnerPaket = (Spinner) findViewById(R.id.outlet_spinner_paket);
        spinnerPewangi = (Spinner) findViewById(R.id.outlet_spinner_pewangi);
        setNameOutlet();
        setSpinnerData();
    }

    private void setNameOutlet() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            nameOutlet = bundle.getString(AppConstants.EXTRA_KEY_NAME_OUTLET);
            addressOutlet = bundle.getString(AppConstants.EXTRA_KEY_ADDRESS_OUTLET);

            getSupportActionBar().setTitle("Request Order");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            nameOutletTextView.setText(nameOutlet);
            addressOutletTextView.setText(addressOutlet);
        }
    }

    private void setSpinnerData() {

        // Spinner click listener
        spinnerPaket.setOnItemSelectedListener(this);
        spinnerPewangi.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> paket = new ArrayList<String>();
        paket.add("Daily (1 hari)");
        paket.add("Normal (3 hari)");
        paket.add("Express (0 hari)");

        List<String> pewangi = new ArrayList<String>();
        pewangi.add("Aroma Apel");
        pewangi.add("Aroma Jeruk");
        pewangi.add("Aroma Theraphy");
        pewangi.add("Lainnya");

        // attaching data adapter to spinnerPaket
        spinnerPaket.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paket));
        spinnerPewangi.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pewangi));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editTextDate:

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            Locale current = getResources().getConfiguration().locale;
                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", current);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", current);

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // Display Selected date in textbox
                                String day = (dayOfMonth < 10) ? "0" + dayOfMonth : String.valueOf(dayOfMonth);
                                String month = ((monthOfYear + 1) < 10) ? "0" + (monthOfYear + 1) : String.valueOf(monthOfYear + 1);

                                String textDate = day + "-" + month + "-" + year;
                                reminderDate = null;
                                try {
                                    reminderDate = formatter.parse(textDate);
                                } catch (ParseException e) {
                                    reminderDate = new Date();
                                }

                                boolean isUpcomingYear = !(year < curYt);
                                boolean isUpcomingMonth = !(year == curYt && monthOfYear < curMt);
                                boolean isUpcomingDate = !(year == curYt && monthOfYear == curMt && dayOfMonth < curDt);
                                if (isUpcomingYear && isUpcomingMonth && isUpcomingDate) {
                                    editTextDate.setText(dateFormat.format(reminderDate));

                                } else {
                                    reminderDate = new Date();
                                    editTextDate.setText(dateFormat.format(new Date()));
                                }

                            }
                        }, curYt, curMt, curDt);
                dpd.show();
                break;
            case R.id.editTextTime:
                Calendar cal = Calendar.getInstance();
                if (reminderDate != null)
                    cal.setTime(reminderDate);
                else
                    cal.setTime(new Date());

                final int reminderDt = cal.get(Calendar.DAY_OF_MONTH);
                final int reminderMt = cal.get(Calendar.MONTH);
                final int reminderYt = cal.get(Calendar.YEAR);

                final int curHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                final int curMinute = Calendar.getInstance().get(Calendar.MINUTE) + 5;

                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour) {
                                mHour = hourOfDay;
                                mMinute = minuteOfHour;

                                boolean isUpcomingYear = !(reminderYt < curYt);
                                boolean isUpcomingMonth = !(reminderYt == curYt && reminderMt < curMt);
                                boolean isUpcomingDate = !(reminderYt == curYt && reminderMt == curMt && reminderDt < curDt);
                                boolean isUpcomingHour = !(reminderYt == curYt && reminderMt == curMt &&
                                        reminderDt == curDt && mHour < curHour);
                                boolean isUpcomingMinute = !(reminderYt == curYt && reminderMt == curMt &&
                                        reminderDt == curDt && mHour == curHour && mMinute < curMinute);


                                if (isUpcomingYear && isUpcomingMonth && isUpcomingDate && isUpcomingHour
                                        && isUpcomingMinute) {
                                    editTextTime.setText(String.format("%02d : %02d", mHour, mMinute));
                                } else {
                                    mHour = curHour;
                                    mMinute = curMinute;

                                    editTextTime.setText(String.format("%02d : %02d", mHour, mMinute));
                                }


                            }
                        }, curHour, curMinute, false);
                tpd.show();
                break;
            case R.id.outlet_call_linear_layout:
                call();
                break;
        }
    }

    private void call() {
        Intent in = new Intent(Intent.ACTION_CALL);
        in.setData(Uri.parse("tel:085286637258"));
        try {
            startActivity(in);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "Unable to call", Toast.LENGTH_SHORT).show();
        }
    }
}
