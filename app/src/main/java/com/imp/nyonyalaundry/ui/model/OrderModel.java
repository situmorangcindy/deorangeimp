package com.imp.nyonyalaundry.ui.model;

/**
 * Created by Owner on 7/5/2016.
 */
public class OrderModel {

    private String orderId;
    private String orderShop;
    private String orderPrice;
    private String orderState;

    public OrderModel(String orderId, String orderShop, String orderPrice, String orderState){
        this.orderId = orderId;
        this.orderShop = orderShop;
        this.orderPrice = orderPrice;
        this.orderState = orderState;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderShop() {
        return orderShop;
    }

    public void setOrderShop(String orderShop) {
        this.orderShop = orderShop;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }
}
