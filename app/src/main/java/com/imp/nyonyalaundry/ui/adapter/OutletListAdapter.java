package com.imp.nyonyalaundry.ui.adapter;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.ui.OutletActivity;
import com.imp.nyonyalaundry.ui.adapter.model.Outlet;

public class OutletListAdapter extends ArrayAdapter<Outlet> {

	private List<Outlet> list;
	private LayoutInflater inflater = null;
	private Activity context;

	public OutletListAdapter(Activity context, List<Outlet> list) {
		super(context, R.layout.item_outlet, list);
		this.list = list;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.list.size();
	}

	@Override
	public Outlet getItem(int position) {
		return list.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (list == null || list.size() <= 0) {
			return view;
		}
		if (view == null) {
			view = inflater.inflate(R.layout.item_outlet, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.container = (RelativeLayout) view.findViewById(R.id.item_outlet_container);
			viewHolder.nameTextView = (TextView) view.findViewById(R.id.item_outlet_name_text_view);
			viewHolder.nameTextView = (TextView) view.findViewById(R.id.item_outlet_name_text_view);
			viewHolder.addressTextView = (TextView) view.findViewById(R.id.item_outlet_address_text_view);
			view.setTag(viewHolder);
		}


		final Outlet item = getItem(position);
		viewHolder = (ViewHolder) view.getTag();
		viewHolder.container.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showOutlet(item.getName(), item.getAddress());
			}
		});
		viewHolder.nameTextView.setText(item.getName());
		viewHolder.addressTextView.setText(item.getAddress());

		return view;
	}

	static class ViewHolder {
		protected RelativeLayout container;
		protected TextView nameTextView;
		protected TextView addressTextView;

	}

	private void showOutlet(String name, String address){
			Intent intent = new Intent(context, OutletActivity.class);
			intent.putExtra(AppConstants.EXTRA_KEY_NAME_OUTLET, name);
			intent.putExtra(AppConstants.EXTRA_KEY_ADDRESS_OUTLET, address);
			context.startActivity(intent);
	}
}
