package com.imp.nyonyalaundry.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.ui.LoginActivity;
import com.imp.nyonyalaundry.helper.Pref;

/**
 * Created by Owner on 6/26/2016.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener{

    private TextView logoutAction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        logoutAction = (TextView) view.findViewById(R.id.setting_logout_action_text_view);
        logoutAction.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.setting_logout_action_text_view:
                processLogout();
                break;
        }
    }

    private void processLogout(){
        FirebaseAuth.getInstance().signOut();
        goToLogin();
    }

    private void goToLogin(){
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
