package com.imp.nyonyalaundry.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.Pref;
import com.imp.nyonyalaundry.ui.fragment.DummyTabContent;
import com.imp.nyonyalaundry.ui.fragment.HelpFragment;
import com.imp.nyonyalaundry.ui.fragment.HistoryFragment;
import com.imp.nyonyalaundry.ui.fragment.HomeFragment;
import com.imp.nyonyalaundry.ui.fragment.IBaseFragment;
import com.imp.nyonyalaundry.ui.fragment.SettingsFragment;
import com.imp.nyonyalaundry.ui.model.User;

/**
 * Created by Owner on 6/26/2016.
 */
public class MainActivity extends FragmentActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    public static final String TAB_0 = "HOME";
    public static final String TAB_1 = "HISTORY";
    public static final String TAB_2 = "HELP";
    public static final String TAB_3 = "SETTINGS";

    private TabHost tabHost;
    private TabHost.TabSpec tab0, tab1, tab2, tab3;
    private int selectedTab = 0;
    private TabHost.OnTabChangeListener tabChangeListener;

    /**
     * Firebase Authentication using a custom minted token. For more information, see:
     * https://firebase.google.com/docs/auth/android/custom-auth
     */
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        setTabHost(selectedTab);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // initialize firebase auth
        mAuth = FirebaseAuth.getInstance();

        //initialize firebase auth_state_listener
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Toast.makeText(MainActivity.this, "onAuthStateChanged:signed_in:" + user.getUid(), Toast.LENGTH_SHORT).show();
                    onAuthSuccess(user);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    Toast.makeText(MainActivity.this, "onAuthStateChanged:signed_out", Toast.LENGTH_SHORT).show();
                    startSignIn();
                }
            }
        };

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void setTabHost(int currentTab){
        tabHost.setup();

        /** Defining Tab Change Listener event. This is invoked when tab is changed */
        tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                android.support.v4.app.FragmentManager fm =   getSupportFragmentManager();

                HomeFragment homeFragment = (HomeFragment) fm.findFragmentByTag(TAB_0);
                HistoryFragment historyFragment = (HistoryFragment) fm.findFragmentByTag(TAB_1);
                HelpFragment helpFragment = (HelpFragment) fm.findFragmentByTag(TAB_2);
                SettingsFragment settingsFragment = (SettingsFragment) fm.findFragmentByTag(TAB_3);

                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();

                /** Detaches the homeFragment if exists */
                if(homeFragment!=null)
                    ft.detach(homeFragment);
                /** Detaches the homeFragment if exists */
                if(historyFragment!=null)
                    ft.detach(historyFragment);
                /** Detaches the homeFragment if exists */
                if(helpFragment!=null)
                    ft.detach(helpFragment);
                /** Detaches the homeFragment if exists */
                if(settingsFragment!=null)
                    ft.detach(settingsFragment);

                /** Detaches the homeFragment if exists */
                if(homeFragment!=null)
                    ft.detach(homeFragment);
                /** Detaches the historyFragment if exists */
                if(historyFragment!=null)
                    ft.detach(historyFragment);
                /** Detaches the helpFragment if exists */
                if(helpFragment!=null)
                    ft.detach(helpFragment);
                /** Detaches the settingsFragment if exists */
                if(settingsFragment!=null)
                    ft.detach(settingsFragment);

                /** If current tab is android */
                if(tabId.equalsIgnoreCase(TAB_0)){

                    if(homeFragment==null){
                        /** Create HomeFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent,new HomeFragment(), TAB_0);
                    }else{
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(homeFragment);
                    }

                }else if(tabId.equalsIgnoreCase(TAB_1)){

                    if(historyFragment==null){
                        /** Create HistoryFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent,new HistoryFragment(), TAB_1);
                    }else{
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(historyFragment);
                    }

                }else if(tabId.equalsIgnoreCase(TAB_2)){

                    if(helpFragment==null){
                        /** Create HelpFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent,new HelpFragment(), TAB_2);
                    }else{
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(helpFragment);
                    }

                }else if(tabId.equalsIgnoreCase(TAB_3)){

                    if(settingsFragment==null){
                        /** Create SettingsFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent,new SettingsFragment(), TAB_3);
                    }else{
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(settingsFragment);
                    }

                }
                ft.commit();
                setColorWidget();
            }
        };

        tabHost.setOnTabChangedListener(tabChangeListener);

        /** Defining tab builder for Home tab */
        tab0 = tabHost.newTabSpec(TAB_0);
        tab0.setIndicator("",null);
        tab0.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tab0);

        /** Defining tab builder for History tab */
        tab1 = tabHost.newTabSpec(TAB_1);
        tab1.setIndicator("",null);
        tab1.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tab1);

        /** Defining tab builder for Help tab */
        tab2 = tabHost.newTabSpec(TAB_2);
        tab2.setIndicator("",null);
        tab2.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tab2);

        /** Defining tab builder for Setting tab */
        tab3 = tabHost.newTabSpec(TAB_3);
        tab3.setIndicator("",null);
        tab3.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tab3);

        setColorWidget();
    }

    private void setColorWidget() {

        tabHost.getTabWidget().setBackgroundResource(R.drawable.shape_tab_grad);
        tabHost.getTabWidget().setDividerDrawable(null);

        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            View v = tabHost.getTabWidget().getChildAt(i);

            if (i == 0) {
                v.setBackgroundResource(R.drawable.ic_home);
            }
            if (i == 1) {
                v.setBackgroundResource(R.drawable.ic_history);
            }
            if (i == 2) {
                v.setBackgroundResource(R.drawable.ic_help);
            }
            if (i == 3) {
                v.setBackgroundResource(R.drawable.ic_settings);
            }
            v.setPadding(0, 10, 0, 10);
        }

        View v = tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()); // selected

        if (tabHost.getCurrentTab() == 0) {
            v.setBackgroundResource(R.drawable.ic_home_selected);
        } else if (tabHost.getCurrentTab() == 1) {
            v.setBackgroundResource(R.drawable.ic_history_selected);
        }else if (tabHost.getCurrentTab() == 2) {
            v.setBackgroundResource(R.drawable.ic_help_selected);
        }else if (tabHost.getCurrentTab() == 3) {
            v.setBackgroundResource(R.drawable.ic_settings_selected);
        }
    }

    private void startSignIn() {
        /**
         * TODO : use firebase customAuth
         */
        signInWithEmailAndPassword();
    }

    private void signInWithEmailAndPassword(){
        String email = Pref.readString(getApplicationContext(), Pref.UID);
        String password = Pref.readString(getApplicationContext(), Pref.TOKEN);

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCustomToken", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",Toast.LENGTH_SHORT).show();
                        }else {
                            Log.d(TAG, "Authentication Success." + task.getResult().getUser().getUid());
                            Toast.makeText(MainActivity.this, "Authentication Success." + task.getResult().getUser().getUid(),
                                    Toast.LENGTH_SHORT).show();

                            onAuthSuccess(task.getResult().getUser());
                        }
                    }
                });
        // [END create_user_with_email]
    }

    private void onAuthSuccess(FirebaseUser user) {
        String uid = Pref.readString(MainActivity.this, Pref.UID);
        String phone = getPhone(uid);

        // Write new user
        writeNewUser(user.getUid(), uid, phone);
    }

    private String getPhone(String uid) {
        if (uid.contains("@")) {
            return uid.split("@")[0];
        } else {
            return uid;
        }
    }

    private void writeNewUser(String firebaseUid, String uid, String phone) {
        User user = new User(uid);
        user.setPhone(phone);

        mDatabase.child("users").child(firebaseUid).setValue(user);
    }
}
