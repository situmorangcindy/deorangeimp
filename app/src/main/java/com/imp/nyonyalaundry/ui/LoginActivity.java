package com.imp.nyonyalaundry.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.helper.Pref;

/**
 * Created by Owner on 6/28/2016.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button loginButton;
    private TextView signUpAction;
    private TextView forgotPasswordAction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        String token = Pref.readString(LoginActivity.this.getApplicationContext(), Pref.TOKEN);
        if (!token.isEmpty()) {
            finish();
            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        loginButton = (Button) findViewById(R.id.login_action_button);
        loginButton.setOnClickListener(this);

        signUpAction = (TextView) findViewById(R.id.login_call_sign_up_text_view);
        signUpAction.setOnClickListener(this);

        forgotPasswordAction = (TextView) findViewById(R.id.login_forgot_password_text_view);
        forgotPasswordAction.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_action_button:
                loginSuccess();
                break;
            case R.id.login_call_sign_up_text_view:
                startActivity(new Intent(this, RegisterActivity.class));
                finish();
                break;
            case R.id.login_forgot_password_text_view:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                finish();
                break;
        }
    }

    private void loginSuccess(){
        goToMain();
    }

    private void goToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
