package com.imp.nyonyalaundry.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.ui.model.OrderModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 6/26/2016.
 */
public class HistoryInProgressFragment extends Fragment {

    private List<OrderModel> orderList = new ArrayList<OrderModel>();
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_history_inprogress, container, false);
        listView = (ListView) view.findViewById(R.id.history_inprogress_list_view);
        setUpData();
        return view;
    }

    private void setUpData(){
        orderList.clear();
        for(int i= 1; i< 10; i++) {
            orderList.add(new OrderModel(String.valueOf(i), "Laundry " + i, String.valueOf(i*10000), "WASH"));
        }
        listView.setAdapter(new OrderAdapter(getActivity()));
    }

    class OrderAdapter extends BaseAdapter {
        LayoutInflater inflater;
        Context context;

        public OrderAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(this.context);
        }

        @Override
        public int getCount() {
            return orderList.size();
        }

        @Override
        public Object getItem(int i) {
            return orderList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            MyViewHolder mViewHolder;

            if (view == null) {
                view = inflater.inflate(R.layout.item_in_progress, viewGroup, false);
                mViewHolder = new MyViewHolder(view);
                view.setTag(mViewHolder);
            } else {
                mViewHolder = (MyViewHolder) view.getTag();
            }

            OrderModel orderModel = orderList.get(position);

            mViewHolder.tvState.setText(orderModel.getOrderState());
            mViewHolder.tvKodePesan.setText(orderModel.getOrderId());

            return view;
        }

        private class MyViewHolder {
            TextView tvState, tvKodePesan;

            public MyViewHolder(View item) {
                tvState = (TextView) item.findViewById(R.id.item_in_progress_order_state_text_view);
                tvKodePesan = (TextView) item.findViewById(R.id.item_in_progress_order_no_text_view);
            }
        }
    }
}
