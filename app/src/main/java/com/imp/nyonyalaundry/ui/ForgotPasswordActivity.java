package com.imp.nyonyalaundry.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.imp.nyonyalaundry.R;

/**
 * Created by Owner on 7/5/2016.
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView loginAction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        loginAction = (TextView) findViewById(R.id.forgot_password_call_sign_in_text_view);
        loginAction.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forgot_password_call_sign_in_text_view:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
    }
}
