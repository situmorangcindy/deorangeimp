package com.imp.nyonyalaundry.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.helper.gps.ConnectionDetector;
import com.imp.nyonyalaundry.helper.gps.GPSTracker;
import com.imp.nyonyalaundry.ui.OutletActivity;
import com.imp.nyonyalaundry.ui.PermissionSettingActivity;
import com.imp.nyonyalaundry.ui.SearchOutletActivity;
import com.imp.nyonyalaundry.ui.TopUpActivity;

/**
 * Created by Owner on 6/26/2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, GoogleMap.OnMarkerClickListener {
    private MapView mapView;
    private TextView searchTextView;
    private LinearLayout topUpAction;
    private GoogleMap map;
    // GPS Location
    private GPSTracker gps;// Connection detector class
    private ConnectionDetector cd;
    // flag for Internet connection status
    private boolean isInternetPresent = false;
    LatLng latLng;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        searchTextView = (TextView) view.findViewById(R.id.home_search_text_view);
        searchTextView.setOnClickListener(this);
        topUpAction = (LinearLayout) view.findViewById(R.id.home_top_up_action_container) ;
        topUpAction.setOnClickListener(this);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) view.findViewById(R.id.home_mapview);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(getActivity(), PermissionSettingActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
        } else {
            cd = new ConnectionDetector(getActivity());

            // Check if Internet present
            isInternetPresent = cd.isConnectingToInternet();
            if (!isInternetPresent) {
                // Internet Connection is not present
                Toast.makeText(getActivity(), "Error. No Internet connection", Toast.LENGTH_SHORT).show();
            } else {
                map = mapView.getMap();
                if (map != null) {
                    // OnClick Event listener for the Google Map
                    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                        @Override
                        public void onMapClick(LatLng point) {

                            generateMarker(point);
                        }
                    });
                    // LongClick Event listener for the Google Map
                    map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                        @Override
                        public void onMapLongClick(LatLng point) {
                            // Clear all the markers from the Google Map
                            map.clear();
                        }
                    });
                    map.setOnMarkerClickListener(this);
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.setMyLocationEnabled(true);

                    // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                    if (isPlayServicesAvailable()) {
                        MapsInitializer.initialize(this.getActivity());
                    }

                    Location location = getLastBestLocation();
                    if (location != null) {
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    } else {
                        latLng = getDeviceLocation();
                    }

                    generateMarker(latLng);
                    // Updates the location and zoom of the MapView
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(AppConstants.LEVEL_MAP_ZOOM).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    map.animateCamera(cameraUpdate);
                }
            }
        }
        return view;
    }

    private Location getLastBestLocation() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            long GPSLocationTime = 0;
            if (null != locationGPS) {
                GPSLocationTime = locationGPS.getTime();
            }

            long NetLocationTime = 0;

            if (null != locationNet) {
                NetLocationTime = locationNet.getTime();
            }

            if (0 < GPSLocationTime - NetLocationTime) {
                return locationGPS;
            } else {
                return locationNet;
            }
        }
    }

    private LatLng getDeviceLocation() {
        // creating GPS Class object
        gps = new GPSTracker(getActivity());

        // check if GPS location can get
        if (gps.canGetLocation()) {
            Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
            return new LatLng(gps.getLatitude(), gps.getLongitude());

        } else {
            gps.showSettingsAlert();
        }
        return new LatLng(0, 0);
    }

    public boolean isPlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                return false;
            }
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String title = marker.getTitle();
        double lat = marker.getPosition().latitude;
        double lng = marker.getPosition().longitude;
        Intent intent = new Intent(getActivity(), OutletActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_NAME_OUTLET, "Laundry Bersih");
        intent.putExtra(AppConstants.EXTRA_KEY_ADDRESS_OUTLET, "Alamat: Jalan " +title);
        getActivity().startActivity(intent);
        return false;
    }

    private void generateMarker(LatLng point) {
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting position for the marker
        markerOptions.position(point);

        // Setting custom icon for the marker
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));

        // Setting title for the infowindow
        markerOptions.title(point.latitude + "," + point.longitude);

        // Adding the marker to the map
        map.addMarker(markerOptions);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.home_search_text_view:
                goToSearchPage();
                break;
            case R.id.home_top_up_action_container:
                getActivity().startActivity(new Intent(getActivity(), TopUpActivity.class));
                break;
        }

    }

    private void goToSearchPage(){
        Intent intent = new Intent(getActivity(), SearchOutletActivity.class);
        startActivity(intent);
    }

}
