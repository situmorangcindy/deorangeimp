package com.imp.nyonyalaundry.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imp.nyonyalaundry.R;
import com.imp.nyonyalaundry.helper.CustomHttpClient;
import com.imp.nyonyalaundry.helper.Pref;
import com.imp.nyonyalaundry.helper.ServiceUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by Owner on 6/28/2016.
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String REQUIRED = "Required";

//    private int type = 0;
//    private TextView typeTextView;
    private Button buttonRegister;
    private TextView signInAction;
    private EditText emailEditText, nameEditText, phoneEditText, passwordEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            type = bundle.getInt(AppConstants.EXTRA_KEY_REGISTER_TYPE);
        }*/

        setContentView(R.layout.activity_register);
//        typeTextView = (TextView) findViewById(R.id.register_type_text_view);
        buttonRegister = (Button) findViewById(R.id.register_action_button);
        buttonRegister.setOnClickListener(this);
        signInAction = (TextView) findViewById(R.id.register_call_sign_in_button);
        signInAction.setOnClickListener(this);

        emailEditText = (EditText) findViewById(R.id.register_input_email_edit_text);
        nameEditText = (EditText) findViewById(R.id.register_input_name_edit_text);
        phoneEditText = (EditText) findViewById(R.id.register_input_phone_edit_text);
        passwordEditText = (EditText) findViewById(R.id.register_input_password_edit_text);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register_action_button:
                doRegistration();
                break;
            case R.id.register_call_sign_in_button:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
    }

    private void doRegistration(){
        if (TextUtils.isEmpty(nameEditText.getText().toString())) {
            nameEditText.setError(REQUIRED);
            return;
        }
        if (TextUtils.isEmpty(emailEditText.getText().toString())) {
            emailEditText.setError(REQUIRED);
            return;
        }
        if (TextUtils.isEmpty(phoneEditText.getText().toString())) {
            phoneEditText.setError(REQUIRED);
            return;
        }
        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordEditText.setError(REQUIRED);
            return;
        }
        new sendPhoneNumber(phoneEditText.getText().toString().trim()).execute();
    }

    private class sendPhoneNumber extends AsyncTask<Void, Void, String> {

        private ProgressDialog pDialog;
        private boolean isSuccess = true;
        private String phoneNumber;

        public sendPhoneNumber(String phone){
            this.phoneNumber = phone.trim().replaceAll("\\D+","");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            phoneNumber = mPhoneEdit.getText().toString().trim().replaceAll("\\D+","");
            pDialog = ProgressDialog.show(RegisterActivity.this, "", "Sending phone number..", true, true);
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    sendPhoneNumber.this.cancel(true);
                }
            });
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("phone", phoneNumber));
//                String obyek_json = CustomHttpClient.executeHttpPost(URLs.sendPhoneNumberURL(), nameValuePairs);
                String obyek_json = CustomHttpClient.executeHttpPost(ServiceUrl.signUp(), nameValuePairs);
                if (!obyek_json.contains("success")) {
                    isSuccess = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                isSuccess = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!isSuccess) {
                Toast.makeText(RegisterActivity.this.getApplicationContext(),
                        "Error on connection!", Toast.LENGTH_SHORT).show();
            } else {
                Pref.saveString(RegisterActivity.this, Pref.UID, String.format("%s@dorange.com",phoneNumber));
                startActivity(new Intent(RegisterActivity.this.getApplicationContext(), VerificationActivity.class));
                RegisterActivity.this.finish();
            }
            pDialog.dismiss();
        }
    }
}
