package com.imp.nyonyalaundry.helper.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.helper.service.SmsVerificationService;

import org.apache.http.util.TextUtils;

/**
 * Created by Owner on 7/7/2016.
 */
public class SmsVerificationReceiver extends BroadcastReceiver{
    private final static String TAG = SmsVerificationService.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object aPdusObj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Log.v(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);

                    // if the SMS is not from our gateway, ignore the message
                    if (!senderAddress.toLowerCase().contains(AppConstants.SMS_ORIGIN.toLowerCase())) {
                        return;
                    }

                    // verification code from sms
                    String verificationCode = getVerificationCode(message);

                    Log.e(TAG, "OTP received: " + verificationCode);

                    Intent smsVerificationIntent = new Intent(context, SmsVerificationService.class);
                    smsVerificationIntent.putExtra(AppConstants.KEY_EXTRA_SMS_VERIFICATION, verificationCode);
                    context.startService(smsVerificationIntent);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }
    private String getVerificationCode(String message) {
        String code = message.replaceAll("[^0-9]+", "");
        if(!TextUtils.isEmpty(code)){
            code.substring(0, 4);
        }
        return code;
    }
}
