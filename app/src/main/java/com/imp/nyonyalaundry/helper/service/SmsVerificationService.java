package com.imp.nyonyalaundry.helper.service;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.imp.nyonyalaundry.helper.AppConstants;
import com.imp.nyonyalaundry.helper.Pref;
import com.imp.nyonyalaundry.ui.MainActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Owner on 7/7/2016.
 */
public class SmsVerificationService extends IntentService {
    private final static String TAG = SmsVerificationService.class.getSimpleName();


    public SmsVerificationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String smsCode = intent.getStringExtra(AppConstants.KEY_EXTRA_SMS_VERIFICATION);
            verifySmsCode(smsCode);
        }
    }

    private void verifySmsCode(final String smsCode) {
        Log.v(TAG, "sms code : " + smsCode);
        new VerifySmsCodeAsyncTask().execute(smsCode);

    }

    private class VerifySmsCodeAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            /*try {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("phone", BerryPreferences.readString(getApplicationContext(), BerryPreferences.PHONE)));
                nameValuePairs.add(new BasicNameValuePair("code", strings[0]));
                String jsonString = CustomHttpClient.executeHttpPost(URLs.smsVerification(), nameValuePairs);
                JSONObject data = new JSONObject(jsonString);
                JSONObject result = data.optJSONObject("result");

                // check if service data exist
                if (result != null) {
                    BerryPreferences.saveBoolean(getApplicationContext(), BerryPreferences.IS_SIGN_IN, true);
                    BerryPreferences.saveString(getApplicationContext(), BerryPreferences.UID, result.optString("uid"));
                    BerryPreferences.saveString(getApplicationContext(), BerryPreferences.TOKEN, result.optString("token"));
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;*/
            /**
             * uncomment above code when api is ready
             */
            String token = UUID.randomUUID().toString();
            Pref.saveString(getApplicationContext(), Pref.TOKEN,  token);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean isSuccess) {
            super.onPostExecute(isSuccess);
            if(isSuccess) {
                Intent intent = new Intent(SmsVerificationService.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }
}
