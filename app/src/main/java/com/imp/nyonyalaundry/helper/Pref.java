package com.imp.nyonyalaundry.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Owner on 6/28/2016.
 */
public class Pref {

    public static final String UID = "pref_uid";
    public static final String TOKEN = "pref_token";
    public static final String IS_FIRST_TIME_LAUNCH = "isFirstTimeLaunch";

    /**
     * Boolean
     * @param context
     * @param key
     * @param value
     */

    public static void saveBoolean(Context context, String key, boolean value) {
        SharedPreferences prefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static boolean readBooleanDefaultFalse(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);
    }

    public static boolean readBooleanDefaultTrue(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, true);
    }

    /**
     * String
     * @param context
     * @param key
     * @param value
     */
    public static void saveString(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String readString(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }
}
