package com.imp.nyonyalaundry.helper;

/**
 * Created by Owner on 6/26/2016.
 */
public interface AppConstants {

    String EXTRA_KEY_REGISTER_TYPE = "EXTRA_KEY_REGISTER_TYPE";
    String EXTRA_KEY_NAME_OUTLET = "EXTRA_KEY_NAME_OUTLET";
    String EXTRA_KEY_ADDRESS_OUTLET = "EXTRA_KEY_ADDRESS_OUTLET";

    String SMS_ORIGIN = "SMS";
    String KEY_EXTRA_SMS_VERIFICATION = "KEY_EXTRA_SMS_VERIFICATION";

    // preference
    String PREF_KEY_LOGIN = "PREF_KEY_LOGIN";

    int KEY_REQUEST_PERMISSION_MARSHMALLOW = 1;
    int LEVEL_MAP_ZOOM = 15;
}
